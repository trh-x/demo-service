#!/bin/sh

psql -h localhost -p 31330 demo-service < delete-schema.pgsql \
  && psql -h localhost -p 31330 demo-service < upsells-schema.pgsql
