CREATE SCHEMA upsells_service;
/* CREATE SCHEMA upsells_service_private; */

CREATE TYPE upsells_service.upsell_asset_type AS ENUM (
  'IMAGE',
  'VIDEO'
);

CREATE TABLE upsells_service.upsell_asset (
  id                SERIAL PRIMARY KEY,
  upsell_asset_type upsells_service.upsell_asset_type NOT NULL,
  file_path         TEXT NOT NULL
);

CREATE TYPE upsells_service.upsell_type AS ENUM (
  'FOOD_AND_BEVERAGE',
  'TICKET_PROTECTION'
);

CREATE TABLE upsells_service.upsell (
  id          SERIAL PRIMARY KEY,
  upsell_type upsells_service.upsell_type NOT NULL,
  details     TEXT NOT NULL CHECK (CHAR_LENGTH(details) < 256),
  price       MONEY NOT NULL,
  created_at  TIMESTAMP DEFAULT NOW()
);

COMMENT ON TABLE upsells_service.upsell IS 'An upsell asset, e.g. an image or video to illustrate the upsell item.';

CREATE TABLE upsells_service.upsell_2_upsell_asset (
  id              SERIAL PRIMARY KEY,
  upsell_id       INTEGER NOT NULL REFERENCES upsells_service.upsell(id),
  upsell_asset_id INTEGER NOT NULL REFERENCES upsells_service.upsell_asset(id),
  meta            JSON
);

CREATE TYPE upsells_service.venue_type AS ENUM (
  'THEATRE',
  'CINEMA'
);

CREATE TABLE upsells_service.venue (
  id          SERIAL PRIMARY KEY,
  venue_type  upsells_service.venue_type NOT NULL,
  address     TEXT,
  created_at  TIMESTAMP DEFAULT NOW()
);

CREATE TABLE upsells_service.venue_2_upsell (
  id          SERIAL PRIMARY KEY,
  venue_id    INTEGER NOT NULL REFERENCES upsells_service.venue(id),
  upsell_id   INTEGER NOT NULL REFERENCES upsells_service.upsell(id),
  meta        JSON
);

CREATE TABLE upsells_service.title (
  id          SERIAL PRIMARY KEY,
  title_name  TEXT,
  producer    TEXT,
  created_at  TIMESTAMP DEFAULT NOW()
);

CREATE TABLE upsells_service.performance (
  id                SERIAL PRIMARY KEY,
  title_id          INTEGER NOT NULL REFERENCES upsells_service.title(id),
  venue_id          INTEGER NOT NULL REFERENCES upsells_service.venue(id),
  performance_date  TIMESTAMP NOT NULL,
  price             MONEY NOT NULL,
  created_at        TIMESTAMP DEFAULT NOW()
);

CREATE TABLE upsells_service.performance_2_upsell (
  id              SERIAL PRIMARY KEY,
  performance_id  INTEGER NOT NULL REFERENCES upsells_service.performance(id),
  upsell_id       INTEGER NOT NULL REFERENCES upsells_service.upsell(id),
  meta            JSON
);

CREATE TABLE upsells_service.customer (
  id            SERIAL PRIMARY KEY,
  first_name    TEXT NOT NULL CHECK (CHAR_LENGTH(first_name) < 80),
  last_name     TEXT CHECK (CHAR_LENGTH(last_name) < 80),
  email         TEXT,
  created_at    TIMESTAMP DEFAULT NOW()
);

CREATE FUNCTION upsells_service.customer_full_name(customer upsells_service.customer) RETURNS TEXT AS $$
  SELECT customer.first_name || ' ' || customer.last_name
$$ LANGUAGE SQL STABLE;

CREATE TABLE upsells_service.purchase (
  id              SERIAL PRIMARY KEY,
  purchase_date   TIMESTAMP DEFAULT NOW(),
  customer_id     INTEGER NOT NULL REFERENCES upsells_service.customer(id),
  performance_id  INTEGER NOT NULL REFERENCES upsells_service.performance(id),
  upsell_id       INTEGER NOT NULL REFERENCES upsells_service.upsell(id)
);

CREATE FUNCTION upsells_service.purchase_price(purchase upsells_service.purchase) RETURNS MONEY AS $$
  SELECT SUM(price)
  FROM (
    SELECT price
    FROM upsells_service.performance
    WHERE id = purchase.performance_id
    UNION ALL
    SELECT price
    FROM upsells_service.upsell
    WHERE id = purchase.upsell_id
    UNION ALL
    SELECT price
    FROM upsells_service.purchase_2_upsell p2u
    LEFT JOIN upsells_service.upsell u
    ON p2u.upsell_id = u.id
    WHERE p2u.purchase_id = purchase.id) pur_price
$$ LANGUAGE SQL STABLE;

CREATE TABLE upsells_service.purchase_2_upsell (
  id          SERIAL PRIMARY KEY,
  purchase_id INTEGER NOT NULL REFERENCES upsells_service.purchase(id),
  upsell_id   INTEGER NOT NULL REFERENCES upsells_service.upsell(id)
);

  /* FOREIGN KEY (purchase_id, upsell_id) REFERENCES purchase(id), upsell(id) */

CREATE FUNCTION upsells_service.graphql_subscription() RETURNS TRIGGER AS $$
DECLARE
  v_process_new BOOL = (TG_OP = 'INSERT' OR TG_OP = 'UPDATE');
  v_process_old BOOL = (TG_OP = 'UPDATE' OR TG_OP = 'DELETE');
  v_event TEXT = TG_ARGV[0];
  v_topic_template TEXT = TG_ARGV[1];
  v_ref_id_colname TEXT = TG_ARGV[2];
  v_record RECORD;
  v_ref_id TEXT;
  v_topic TEXT;
  v_i INT = 0;
  v_last_topic TEXT;
BEGIN
  FOR v_i IN 0..1 LOOP
    IF (v_i = 0) AND v_process_new IS TRUE THEN
      v_record = NEW;
    ELSIF (v_i = 1) AND v_process_old IS TRUE THEN
      v_record = OLD;
    ELSE
      CONTINUE;
    END IF;
    IF v_ref_id_colname IS NOT NULL THEN
      EXECUTE 'SELECT $1.' || QUOTE_IDENT(v_ref_id_colname)
        USING v_record
        INTO v_ref_id;
    END IF;
    IF v_ref_id IS NOT NULL THEN
      v_topic = REPLACE(v_topic_template, '$1', v_ref_id);
    ELSE
      v_topic = v_topic_template;
    END IF;
    IF v_topic IS DISTINCT FROM v_last_topic THEN
      -- This if statement prevents us from triggering the same notification twice
      v_last_topic = v_topic;
      PERFORM PG_NOTIFY(v_topic, JSON_BUILD_OBJECT(
        'event', v_event,
        'row', ROW_TO_JSON(v_record)
      )::TEXT);
    END IF;
  END LOOP;
  RETURN v_record;
END;
$$ LANGUAGE PLPGSQL VOLATILE SET SEARCH_PATH FROM CURRENT;

CREATE TRIGGER _500_gql_update_purchase
  AFTER INSERT OR UPDATE OR DELETE ON upsells_service.purchase
  FOR EACH ROW
  EXECUTE PROCEDURE upsells_service.graphql_subscription('purchaseChanged', 'purchase:$1', 'performance_id');

 /* AFTER INSERT OR UPDATE OR DELETE ON upsells_service.log_item */
