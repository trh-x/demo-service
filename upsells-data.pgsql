INSERT INTO upsells_service.customer (first_name, last_name) VALUES
('Sonia', 'Manfried')
;

INSERT INTO upsells_service.title (id, title_name, producer) VALUES
(1, 'Iliad', 'Homer')
;

INSERT INTO upsells_service.venue (id, venue_type, address) VALUES
(1, 'THEATRE', '221b Baker Street')
;

INSERT INTO upsells_service.performance (title_id, venue_id, performance_date, price) VALUES
(1, 1, '2019-12-06 16:55:06.867767', 0.10)
;

INSERT INTO upsells_service.upsell (upsell_type, details, price) VALUES
('FOOD_AND_BEVERAGE', 'Glass of Cabernet Sauvignon', 0.42)
;

INSERT INTO upsells_service.upsell_asset (upsell_asset_type, file_path) VALUES
('IMAGE', '/foo/bar')
;
