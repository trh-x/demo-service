INSERT INTO demo_service.person (id, person_type, first_name, last_name) VALUES
(1, 'CUSTOMER', 'Jim', 'Beam'),
(2, 'CUSTOMER', 'Jack', 'Daniels'),
(3, 'CUSTOMER', 'Harley', 'Davidson')
;

INSERT INTO demo_service.task (person_id, body) VALUES
(1, 'Customer needs a new headlight for his Cafe Racer Kawasaki W650.'),
(2, 'This customer has a requirement to slip slowly under the bar.'),
(3, 'The customer''s pannier bag has split, leading to Jim Beam leaking over the floor. A new bag is required.')
;
