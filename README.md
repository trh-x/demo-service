# Tech Anvil - Demo Service

~~An example of a simple Node service, using Apollo GraphQL.~~

~~Work In Progress!~~

Postgraphile GraphQL API from Innovation Day.

## Run

### `npm start`

Runs the server - [http://localhost:4000](http://localhost:4000)

### `npm run postgraphile`

Runs Postgraphile! Prerequisite: DB instance & schema setup - see `./kube/` and `./reload-schema.sh`.

Postgraphile will expose a GraphiQL interface at [http://localhost:5432/graphiql](http://localhost:5432/graphiql).

## Test

### `npm run test`

Note: test suite is still TODO!

## Build

Note: Also still TODO.

### `npm run build`

Bundles the app to `./build`.
