import express from 'express';
import cors from 'cors';
import { postgraphile, makePluginHook } from 'postgraphile';
import PgPubsub from '@graphile/pg-pubsub';
import PgSimplifyInflectorPlugin from '@graphile-contrib/pg-simplify-inflector';
import postgraphPlugin from './postgraph-plugin';

const DATABASE_URL = 'postgres://localhost:31330/demo-service';
const PORT = 5432;
const SCHEMA_NAME = 'upsells_service';

const pluginHook = makePluginHook([PgPubsub]);

const postgraphileOptions = {
  appendPlugins: [PgSimplifyInflectorPlugin, postgraphPlugin],
  enhanceGraphiql: true,
  graphiql: true,
  pluginHook,
  subscriptions: true,
  watchPg: true,

  /*
  // simpleSubscriptions: true,
  websocketMiddlewares: [
    // Add whatever middlewares you need here, note that they should only
    // manipulate properties on req/res, they must not sent response data. e.g.:
    //
    //   require('express-session')(),
    //   require('passport').initialize(),
    //   require('passport').session(),
  ],
  */
};

const app = express();
app.use(cors());
app.use(postgraphile(DATABASE_URL, SCHEMA_NAME, postgraphileOptions));
app.listen(PORT);

