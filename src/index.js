import { ApolloServer, gql } from 'apollo-server';

import { tasks, customers } from './taskData.json';

const typeDefs = gql`
  # scalar Date

  enum LogType {
    CALL
    NOTE
  }

  type User {
    id: ID!
    email: String!
    password: String!
  }

  type Customer {
    id: ID!
    name: String!
  }

  type Task {
    id: ID!
    customer: Customer!
    taskBody: String!
    date: String!
    # date: Date!
    log: [LogItem]
  }

  type LogItem {
    id: ID!
    taskId: ID!
    logType: LogType!
    details: String!
    # user: User!
    userId: String!
    date: String!
    # date: Date!
  }

  type Query {
    tasks: [Task!]
    task(id: ID!): Task
  }

  type Mutation {
    addLogItem(
      taskId: ID!
      userId: ID!
      logType: LogType!
      details: String!
    ): LogItem
  }
`;

function populateId(id, task) {
  return { id, ...task };
}

const resolvers = {
  Query: {
    tasks() {
      return Object.entries(tasks).map(([ id, task ]) => populateId(id, task));
    },
    task(_, { id }) {
      return populateId(id, tasks[id]);
    }
  },

  Mutation: {
    addLogItem(_, { taskId, userId, logType, details }) {
      const logItem = { taskId, userId, logType, details, date: new Date().getTime() };
      tasks[taskId].log.push(logItem);
      return logItem;
    }
  },

  Task: {
    customer({ customerId }) {
      return populateId(customerId, customers[customerId]);
    }
  }
};

const server = new ApolloServer({
  typeDefs,
  resolvers,
  tracing: true,
  engine: {
    debugPrintReports: true
  },
  formatError(error) {
    console.log(error);
    return error;
  },
  formatResponse(response) {
    console.log(response);
    return response;
  }
});

server.listen().then(({ url }) => {
  console.log(`Server ready at ${url}`);
});
