import { makeExtendSchemaPlugin, gql, embed } from 'graphile-utils';

const purchaseTopic = ({ performanceId }) => {
  console.log(`HELOOOO:${performanceId}`);
  return `purchase:${performanceId}`;
}

// const currentUserTopicFromContext = (_args, context, _resolveInfo) => {
//   if (context.jwtClaims.user_id) {
//     return `graphql:user:${context.jwtClaims.user_id}`;
//   } else {
//     throw new Error("You're not logged in");
//   }
// };
      // purchaseAdded: PurchasePayload @pgSubscription(topic: ${embed(
      //   currentUserTopicFromContext
      // )})

export default makeExtendSchemaPlugin(({ pgSql: sql }) => ({
  typeDefs: gql`
    type PurchasePayload {
      # This is populated by our resolver below
      purchase: Purchase
      # This is returned directly from the PostgreSQL subscription payload (JSON object)
      event: String
    }
    extend type Subscription {
      """
      Triggered when a new item is added to the log.
      """
      purchaseAdded(performanceId: Int!): PurchasePayload @pgSubscription(topic: ${embed(purchaseTopic)})
    }
  `,
  resolvers: {
    PurchasePayload: {
      async purchase(
        event,
        _args,
        _context,
        { graphile: { selectGraphQLResultFromTable } }
      ) {
        console.log('EVENT', event);
        const rows = await selectGraphQLResultFromTable(
          sql.fragment`upsells_service.purchase`,
          (tableAlias, sqlBuilder) => {
            sqlBuilder.where(
              sql.fragment`${tableAlias}.id = ${sql.value(
                parseInt(event.row.id, 10)
              )}`
            );
          }
        );
        console.log('ROWS[0]', rows[0]);
        return rows[0];
      },
    },
  },
}));
