module.exports = function(api) {
  const isTest = api.env('test');
  api.cache(true);

  const config = {
    presets: [
      [
        '@babel/preset-env',
        {
          targets: {
            esmodules: true,
            node: true
          }
        }
      ]
    ]
  };

  if (!isTest) config.ignore = ['**/*.spec.js'];

  return config;
}
