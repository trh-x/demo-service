CREATE SCHEMA demo_service;
CREATE SCHEMA demo_service_private;

CREATE TYPE demo_service.person_type AS ENUM (
  'ADMIN',
  'CUSTOMER'
);

CREATE TABLE demo_service.person (
  id          SERIAL PRIMARY KEY,
  person_type demo_service.person_type NOT NULL,
  first_name  TEXT NOT NULL CHECK (CHAR_LENGTH(first_name) < 80),
  last_name   TEXT CHECK (CHAR_LENGTH(last_name) < 80),
  about       TEXT,
  created_at  TIMESTAMP DEFAULT NOW()
);

COMMENT ON TABLE demo_service.person IS 'A person.';

CREATE FUNCTION demo_service.person_full_name(person demo_service.person) RETURNS TEXT AS $$
  SELECT person.first_name || ' ' || person.last_name
$$ LANGUAGE SQL STABLE;

CREATE TABLE demo_service.task (
  id          SERIAL PRIMARY KEY,
  person_id   INTEGER NOT NULL REFERENCES demo_service.person(id),
  body        TEXT,
  created_at  TIMESTAMP DEFAULT NOW()
);

CREATE TYPE demo_service.log_type AS ENUM (
  'CALL',
  'NOTE'
);

CREATE TABLE demo_service.log_item (
  id          SERIAL PRIMARY KEY,
  task_id     INTEGER NOT NULL REFERENCES demo_service.task(id),
  person_id   INTEGER NOT NULL REFERENCES demo_service.person(id),
  log_type    demo_service.log_type,
  details     TEXT,
  created_at  TIMESTAMP DEFAULT NOW()
);

CREATE TABLE demo_service.task_2_log_item (
  id          SERIAL PRIMARY KEY,
  task_id     INTEGER NOT NULL REFERENCES demo_service.task(id),
  log_item_id INTEGER NOT NULL REFERENCES demo_service.log_item(id),
  FOREIGN KEY (task_id, log_item_id) REFERENCES task, log_item (id, id)
);


CREATE FUNCTION demo_service.graphql_subscription() RETURNS TRIGGER AS $$
DECLARE
  v_process_new BOOL = (TG_OP = 'INSERT' OR TG_OP = 'UPDATE');
  v_process_old BOOL = (TG_OP = 'UPDATE' OR TG_OP = 'DELETE');
  v_event TEXT = TG_ARGV[0];
  v_topic_template TEXT = TG_ARGV[1];
  v_ref_id_colname TEXT = TG_ARGV[2];
  v_record RECORD;
  v_ref_id TEXT;
  v_topic TEXT;
  v_i INT = 0;
  v_last_topic TEXT;
BEGIN
  FOR v_i IN 0..1 LOOP
    IF (v_i = 0) AND v_process_new IS TRUE THEN
      v_record = NEW;
    ELSIF (v_i = 1) AND v_process_old IS TRUE THEN
      v_record = OLD;
    ELSE
      CONTINUE;
    END IF;
    IF v_ref_id_colname IS NOT NULL THEN
      EXECUTE 'SELECT $1.' || QUOTE_IDENT(v_ref_id_colname)
        USING v_record
        INTO v_ref_id;
    END IF;
    IF v_ref_id IS NOT NULL THEN
      v_topic = REPLACE(v_topic_template, '$1', v_ref_id);
    ELSE
      v_topic = v_topic_template;
    END IF;
    IF v_topic IS DISTINCT FROM v_last_topic THEN
      -- This if statement prevents us from triggering the same notification twice
      v_last_topic = v_topic;
      PERFORM PG_NOTIFY(v_topic, JSON_BUILD_OBJECT(
        'event', v_event,
        'row', ROW_TO_JSON(v_record)
      )::TEXT);
    END IF;
  END LOOP;
  RETURN v_record;
END;
$$ LANGUAGE PLPGSQL VOLATILE SET SEARCH_PATH FROM CURRENT;

CREATE TRIGGER _500_gql_update_log_item
  AFTER INSERT OR UPDATE OR DELETE ON demo_service.log_item
  FOR EACH ROW
  EXECUTE PROCEDURE demo_service.graphql_subscription('logItemChanged', 'greetz:$1', 'task_id');

# AFTER INSERT OR UPDATE OR DELETE ON demo_service.log_item
