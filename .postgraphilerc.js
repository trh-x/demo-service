module.exports = {
  options: {
    connection: 'postgres://localhost:31330/demo-service',
    enhanceGraphiql: true,
    plugins: ['@graphile/pg-pubsub'],
    schema: ['demo_service'],
    simpleSubscriptions: true,
    subscriptions: true,
    watch: true,
  },
};
